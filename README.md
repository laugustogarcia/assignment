# Rabobank Assigment

## Running the unit tests

In the project's root folder, run `mvn test`.

## Running the integration tests

In the project's root folder, run `mvn verify`.

## Building and running the project

In the project's root folder, run `mvn clean package && docker-compose up --build` (Windows).

## The API

### List all persons

Send a GET request to http://localhost:8080/person

### Get a person by his/her ID

Send a GET request to http://localhost:8080/person/{id}
> This endpoint is not public and requires HTTP Basic Authentication. The authenticated user must possess the ADMIN role. Try admin/admin.

### Save a new person

Send a POST request to http://localhost:8080/person with a JSON body. You can use the following as template:

```javascript
{
    "firstName": "Luiz",
    "lastName": "Silva",
    "birthDay": "2019-12-08T18:55:16.198+0000",
    "address": "Rua da Escola 32, Sintra, Portugal"
}
```

### Update a person's address

Send a PATCH request to http://localhost:8080/person/{id} with a JSON body. You can use the following as template:

```javascript
{
    "address": "Any street, Netherlands"
}
```
> This endpoint will only update the `address` field. Any other field other than `address` will be ignored.
