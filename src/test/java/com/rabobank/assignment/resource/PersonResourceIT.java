package com.rabobank.assignment.resource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.function.Predicate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.rabobank.assignment.model.Person;
import com.rabobank.assignment.repository.PersonRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class PersonResourceIT {

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private PersonRepository userRepository;

	@Autowired
	private EntityManagerFactory entityManagerFactory;

	private final Person juliana = new Person("Juliana", "Capella",
			Date.from(LocalDate.parse("1983-09-13").atStartOfDay(ZoneId.systemDefault()).toInstant()),
			"Escola street 32, Sintra, Portugal");

	private final Person luiz = new Person("Luiz", "Silva",
			Date.from(LocalDate.parse("1983-01-14").atStartOfDay(ZoneId.systemDefault()).toInstant()),
			"Escola street 32, Sintra, Portugal");

	@After
	public void cleanup() {
		final EntityManager em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		em.createNativeQuery("truncate table Person").executeUpdate();
		em.getTransaction().commit();
	}

	@Test
	public void testFindAll() {
		userRepository.save(luiz);
		userRepository.save(juliana);

		final Person[] savedPersons = restTemplate.getForObject("/person", Person[].class);
		assertThat(savedPersons).isNotEmpty().hasSize(2).anyMatch(with(luiz)).anyMatch(with(juliana));
	}

	@Test
	public void testFindByFirstLastNames() {
		userRepository.save(luiz);

		final Person[] personByFirstLastNames = restTemplate.getForObject("/person?firstName=Luiz&lastName=Silva",
				Person[].class);
		assertThat(personByFirstLastNames).isNotEmpty().hasSize(1).anyMatch(with(luiz));
	}

	@Test
	public void testFindByFirstLastNamesReturningError() {
		assertEquals(HttpStatus.BAD_REQUEST,
				restTemplate.getForEntity("/person?firstName=Luiz", ErrorResponse.class).getStatusCode());
		assertEquals(HttpStatus.BAD_REQUEST,
				restTemplate.getForEntity("/person?lastName=Silva", ErrorResponse.class).getStatusCode());
	}

	@Test
	public void testFindOne() throws Exception {
		final Person savedPerson = userRepository.save(luiz);
		assertThat(luiz).matches(with(restTemplate.withBasicAuth("admin", "admin").getForObject("/person/{id}",
				Person.class, savedPerson.getId())));
	}

	@Test
	public void testFindOneNotFound() throws Exception {
		assertEquals(HttpStatus.NOT_FOUND, restTemplate.withBasicAuth("admin", "admin")
				.getForEntity("/person/{id}", Person.class, 1).getStatusCode());
	}

	@Test
	public void testSave() throws Exception {
		assertThat(luiz).matches(with(restTemplate.postForObject("/person", luiz, Person.class)));
	}

//  Spring seems to allows unique constraint violation during tests. 
//	There are a couple of stories about this on the internet.
//	Solving this issue would require investigation time. 

//	@Test
//	@Transactional(propagation = Propagation.NOT_SUPPORTED)
//	public void testSaveDuplicatedPerson() throws Exception {
//		userRepository.save(luiz);
//		assertEquals(HttpStatus.CONFLICT,
//				restTemplate.postForEntity("/person", luiz, ErrorResponse.class).getStatusCode());
//	}

	@Test
	public void testUpdateAddress() {
		// Fix to make Spring's RestTemplate be able to request PATCH endpoints
		// By default it uses java.net stuff which in turn is unable to make PATCH
		// requests, thus making it to use apache's http-client instead.
		final RestTemplate template = restTemplate.getRestTemplate();
		final CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		template.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));

		final Person saved = userRepository.save(luiz);
		final Person patch = new Person("Luiz", "Silva",
				Date.from(LocalDate.parse("1990-01-01").atStartOfDay(ZoneId.systemDefault()).toInstant()),
				"Any street, Netherlands");
		final Person patched = restTemplate.patchForObject("/person/{id}", patch, Person.class, saved.getId());
		assertEquals(patch.getAddress(), patched.getAddress());
		assertNotEquals(patch.getBirthDay(), patched.getBirthDay());
	}

	private Predicate<Person> with(final Person person) {
		return p -> p.getFirstName().equals(person.getFirstName()) && p.getLastName().equals(person.getLastName())
				&& p.getBirthDay().equals(person.getBirthDay()) && p.getAddress().equals(person.getAddress());
	}
}
