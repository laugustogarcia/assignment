package com.rabobank.assignment.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataIntegrityViolationException;

import com.rabobank.assignment.model.Person;
import com.rabobank.assignment.repository.PersonRepository;

@RunWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)
class PersonServiceTest {

	@Mock
	private PersonRepository personRepository;

	@InjectMocks
	private PersonService personService;

	@Captor
	private ArgumentCaptor<Person> captor;

	private final Person juliana = new Person("Juliana", "Capella",
			Date.from(LocalDate.parse("1983-09-13").atStartOfDay(ZoneId.systemDefault()).toInstant()),
			"Escola street 32, Sintra, Portugal");

	private final Person luiz = new Person("Luiz", "Silva",
			Date.from(LocalDate.parse("1983-01-14").atStartOfDay(ZoneId.systemDefault()).toInstant()),
			"Escola street 32, Sintra, Portugal");

	@Test
	void testFindByFirstNameAndLastName() {
		when(personRepository.findByFirstNameAndLastName("Luiz", "Silva")).thenReturn(Collections.singletonList(luiz));
		assertThat(personService.findByFirstNameAndLastName("Luiz", "Silva")).isNotEmpty().hasSize(1)
				.allMatch(p -> p == luiz);
	}

	@Test
	void testFindAll() {
		when(personRepository.findAll()).thenReturn(Arrays.asList(luiz, juliana));
		assertThat(personService.findAll()).isNotEmpty().hasSize(2).anyMatch(p -> p == luiz)
				.anyMatch(p -> p == juliana);
	}

	@Test
	void testFindById() {
		when(personRepository.findById(1L)).thenReturn(Optional.of(luiz));
		assertThat(personService.findById(1L)).isSameAs(luiz);
	}

	@Test
	void testFindByIdNotFound() {
		when(personRepository.findById(1L)).thenReturn(Optional.empty());
		assertThrows(PersonNotFoundException.class, () -> personService.findById(1L));
	}

	@Test
	void testSave() {
		personService.save(luiz);
		verify(personRepository, times(1)).save(captor.capture());
		assertThat(captor.getValue()).isSameAs(luiz);
	}

	@Test
	void testSaveDuplicatedPerson() {
		when(personRepository.save(any())).thenThrow(DataIntegrityViolationException.class);
		assertThrows(PersonUniquenessViolationException.class, () -> personService.save(luiz));
	}

	@Test
	void testUpdateAddress() {
		final Person luiz = new Person("Luiz", "Silva",
				Date.from(LocalDate.parse("1983-01-14").atStartOfDay(ZoneId.systemDefault()).toInstant()),
				"Escola street 32, Sintra, Portugal");

		when(personRepository.findById(1L)).thenReturn(Optional.of(luiz));
		when(personRepository.save(luiz)).thenReturn(luiz);

		personService.updateAddress(1L, "Any street, Netherlands");

		verify(personRepository, times(1)).save(captor.capture());
		assertThat(captor.getValue()).isSameAs(luiz);
		assertEquals("Any street, Netherlands", luiz.getAddress());
	}
	
	@Test
	void testUpdateAddressPersonNotFound() {
		when(personRepository.findById(1L)).thenReturn(Optional.empty());
		assertThrows(PersonNotFoundException.class, () -> personService.updateAddress(1L, "Any street, Netherlands"));
	}
}
