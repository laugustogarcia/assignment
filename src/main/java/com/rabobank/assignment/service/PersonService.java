package com.rabobank.assignment.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.rabobank.assignment.model.Person;
import com.rabobank.assignment.repository.PersonRepository;

@Service
public class PersonService {

	@Autowired
	private PersonRepository personRepository;

	public List<Person> findByFirstNameAndLastName(String firstName, String lastName) {
		return personRepository.findByFirstNameAndLastName(firstName, lastName);
	}

	public List<Person> findAll() {
		return personRepository.findAll();
	}

	public Person findById(Long id) {
		return personRepository.findById(id).orElseThrow(PersonNotFoundException::new);
	}

	public Person save(Person person) {
		try {
			return personRepository.save(person);
		} catch (DataIntegrityViolationException ex) {
			throw new PersonUniquenessViolationException();
		}
	}

	public Person updateAddress(Long id, String newAddress) {
		final Optional<Person> person = personRepository.findById(id);

		if (person.isPresent()) {
			person.get().setAddress(newAddress);
			return personRepository.save(person.get());
		}

		throw new PersonNotFoundException();
	}
}
