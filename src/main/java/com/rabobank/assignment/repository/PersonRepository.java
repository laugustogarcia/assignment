package com.rabobank.assignment.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rabobank.assignment.model.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

	Optional<Person> findById(Long id);

	List<Person> findAll();

	Person save(Person person);

	List<Person> findByFirstNameAndLastName(String firstName, String lastName);
}
