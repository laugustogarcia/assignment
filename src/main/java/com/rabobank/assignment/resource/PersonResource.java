package com.rabobank.assignment.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rabobank.assignment.model.Person;
import com.rabobank.assignment.service.PersonService;

@RestController
@RequestMapping("/person")
public class PersonResource {

	@Autowired
	private PersonService personService;

	@GetMapping
	public List<Person> find(@RequestParam Optional<String> firstName, @RequestParam Optional<String> lastName) {
		if (firstName.isPresent() && lastName.isPresent()) {
			return personService.findByFirstNameAndLastName(firstName.get(), lastName.get());
		} else if (firstName.isPresent() || lastName.isPresent()) {
			throw new BadParamsException();
		}
		return personService.findAll();
	}

	@GetMapping("/{id}")
	public Person findOne(@PathVariable Long id) {
		return personService.findById(id);
	}

	@PostMapping
	public Person save(@RequestBody Person person) {
		return personService.save(person);
	}

	@PatchMapping("/{id}")
	public Person updateAddress(@PathVariable Long id, @RequestBody Person person) {
		// only the address is patched
		return personService.updateAddress(id, person.getAddress());
	}
}
