package com.rabobank.assignment.resource;

public class ErrorResponse {
	// We could have other fields here such as error code, severity, etc
	private String message;

	public ErrorResponse() {
		// empty
	}

	public ErrorResponse(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}