package com.rabobank.assignment.resource;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.rabobank.assignment.service.PersonNotFoundException;
import com.rabobank.assignment.service.PersonUniquenessViolationException;

@ControllerAdvice
public class ExceptionHandler extends ResponseEntityExceptionHandler {

	@org.springframework.web.bind.annotation.ExceptionHandler(value = { PersonNotFoundException.class })
	public ResponseEntity<ErrorResponse> handlePersonNotFound(PersonNotFoundException ex) {
		return new ResponseEntity<ErrorResponse>(new ErrorResponse("Person not found"), new HttpHeaders(),
				HttpStatus.NOT_FOUND);
	}

	@org.springframework.web.bind.annotation.ExceptionHandler(value = { PersonUniquenessViolationException.class })
	public ResponseEntity<ErrorResponse> handlePersonUniquenessVialotion(PersonUniquenessViolationException ex) {
		return new ResponseEntity<ErrorResponse>(
				new ErrorResponse("An person already exists with the provided 'firstName' and 'lastName'"),
				new HttpHeaders(), HttpStatus.CONFLICT);
	}

	@org.springframework.web.bind.annotation.ExceptionHandler(value = { BadParamsException.class })
	public ResponseEntity<ErrorResponse> handleBadParamsException(BadParamsException e) {
		return new ResponseEntity<ErrorResponse>(
				new ErrorResponse("Please provide both 'firstName' and 'lastName' query params"), new HttpHeaders(),
				HttpStatus.BAD_REQUEST);
	}
}
